<?php

function complex_taxonomy_views_data_alter(&$data) {

  $data['node']['complex_taxonomy'] = array(
    'real field' => 'nid',
    'title' => t('Complex taxonomy filter'),
    'description' => t('Filter nodes by an arbitrary number of taxonomies'),
    'argument' => array(
      'label' => t('Complex taxonomy'),
      'help' => t('Filter nodes by a complex taxonomy term expression.'),
      'handler' => 'complex_taxonomy_argument',
      
    ),
  );
  
}

class complex_taxonomy_argument extends views_handler_argument_many_to_one {
  public function query($groub_by = false) {
    $arguments = $this->parseArgument($this->argument);
    $subquery = $this->buildQuery($arguments);
    $this->query->add_where_expression(0, "node.$this->real_field IN(SELECT x.nid FROM ($subquery) x)");
  }

  private function buildQuery($arguments) {
    $queries = array();
    $arguments = array_slice($arguments, 0, COMPLEX_TAXONOMY_JOIN_LIMIT);
    foreach ($arguments as $query_array) {
      $queries[] = $this->buildJoinQuery($query_array);
    }
    $query = implode(' UNION ', $queries);
    return $query;
  }

  private function buildJoinQuery($query_array) {
    $query = 'SELECT t.nid FROM {taxonomy_index} t';
    $join_array = array_shift($query_array);
    $in = $this->extractTids($join_array);
    $where = "t.tid IN($in)";
    foreach ($query_array as $id => $join_array) {
        $in = $this->extractTids($join_array);
        $query .= " INNER JOIN {taxonomy_index} t$id ON t.nid=t$id.nid AND t$id.tid IN ($in)";
    }
    return "($query WHERE $where)";
  }

  private function extractTids($terms) {
    return implode(',', $terms);
  }

  private function parseArgument($argument) {
    /*
     * argument is of the following format
     * x0::x1,x2|x3|… => ( x0 ∧ (x1 ∨ x2) ) ∨ ( x3 ) => ( IN(x0) JOIN IN(x1, x2) ) UNION ( IN(x3) )
     * where
     *  - xX are tids
     */

    $unions = array();
    foreach (explode('|', $argument) as $query_part) {
      $joins = array();
      foreach (explode('::', $query_part) as $join_part) {
        $terms = array();
        $this->last_vocab = '';
        foreach (explode(',', $join_part) as $term_part) {
          $terms[] = (int) $term_part;
        }
        if ($terms) {
          $joins[] = $terms;
        }
      }
      if ($joins) {
        $unions[] = $joins;
      }
    }
    return $unions;

  }

}

